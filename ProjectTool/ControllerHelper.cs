﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using XXF.Api;
using XXF.BasicService.MonitorPlatform.Model;
using XXF.Db;
using XXF.Log;

namespace XXF.ProjectTool
{
    /// <summary>
    /// 控制器帮助类
    /// </summary>
    public class ControllerHelper
    {
        /// <summary>
        /// 普通网站开发时使用
        /// 有错误信息往外抛
        /// 备注：若项目有特殊需要，可根据项目需求拷贝代码到自己项目中自定义。
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="action"></param>
        /// <returns></returns>
        public static T Visit<T>(Func<Db.DbConn, T> action)
        {
            string url = "";
            if (System.Web.HttpContext.Current != null)
            {
                url = System.Web.HttpContext.Current.Request.RawUrl.ToString();
            }
            TimeWatchLog watch = new TimeWatchLog();
            T r = default(T);
            try
            {
                using (Db.DbConn PubConn = DbConfig.CreateConn())
                {
                    PubConn.Open();
                    r = action.Invoke(PubConn);
                }
            }
            catch (Exception exp)
            {
                ErrorLog.Write(url+"调用错误:" + exp.Message, exp);
                throw exp;
            }
            watch.Write(new TimeWatchLogInfo() { logtag = url.GetHashCode(), msg = url, logtype = BaseService.Monitor.SystemRuntime.EnumTimeWatchLogType.ApiUrl });
            return r;
        }
        /// <summary>
        /// Api项目接入时使用,封装api项目接入时候的错误检测及ServerResult封装
        /// 屏蔽错误信息以serverresult约定输出
        /// 备注：若项目有特殊需要，可根据项目需求拷贝代码到自己项目中自定义。
        /// </summary>
        /// <param name="action"></param>
        /// <returns></returns>
        public static ActionResult Visit(Func<Db.DbConn, object> action)
        {
            var json = new JsonResult();

            string url = "";
            if (System.Web.HttpContext.Current != null)
            {
                url = System.Web.HttpContext.Current.Request.RawUrl.ToString();
            }

            TimeWatchLog watch = new TimeWatchLog();
            try
            {
                using (Db.DbConn PubConn = DbConfig.CreateConn())
                {
                    PubConn.Open();
                    var r = action.Invoke(PubConn);
                    //如果是列表类型的，需要记录总记录数和列表集合
                    if (r.GetType() == typeof(ListResult<>))
                    {
                        dynamic r1 = r;
                        json.Data = new ServerResult() { code = 1, msg = "", response = r1.List, total = r1.Total };
                    }
                    else if (r is ServerResult)//为了兼容api原先的返回非标准结果方式
                    {
                        json.Data = r as ServerResult;
                    }
                    else
                    {
                        json.Data = new ServerResult() { code = 1, msg = "", response = r, total = 0 };
                    }
                }
            }
            catch (Exception exp)
            {
                ErrorLog.Write(url+"调用错误:" + exp.Message, exp);
                //异常返回
                json.Data = new ServerResult() { code = -1, msg = exp.Message, response = null, total = 0 };
            }
            watch.Write(new TimeWatchLogInfo() { logtag = url.GetHashCode(), msg = url, logtype = BaseService.Monitor.SystemRuntime.EnumTimeWatchLogType.ApiUrl });
           // json.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return json;
        }
    }
}
