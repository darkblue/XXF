﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using Webdiyer.WebControls.Mvc;
using XXF.Api;
using XXF.ProjectTool;

namespace XXF.Base
{
    /// <summary>
    /// 常用项目controller层公用方法封装
    /// </summary>
    public class BaseCommonController : Controller
    {
        /// <summary>
        /// 获取api分页结果(项目常用ajaxpager分页方式)
        /// </summary>
        /// <param name="result"></param>
        /// <returns></returns>
        public ActionResult GetPagerResult<T>(ClientResult result, int pno, int pagesize)
        {
            if (result.code == 1)
            {
                var list = ApiHelper.Response(result);
                PagedList<T> pagelist = new Webdiyer.WebControls.Mvc.PagedList<T>(list, pno, pagesize, (int)result.total);
                if (Request.IsAjaxRequest())
                    return PartialView("List", pagelist);
                return View(pagelist);
            }
            return View();
        }
        /// <summary>
        /// 默认返回dynamic类型
        /// </summary>
        /// <param name="result"></param>
        /// <param name="pno"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public ActionResult GetPagerResult(ClientResult result, int pno, int pagesize)
        {
            return GetPagerResult<dynamic>(result, pno, pagesize);
        }
        /// <summary>
        /// 默认返回dynamic类型，
        /// 但支持自动生成model类型说明文档至Models/auto/目录下,包含到项目中即可做model使用
        /// </summary>
        /// <param name="result"></param>
        /// <param name="pno"></param>
        /// <param name="pagesize"></param>
        /// <returns></returns>
        public ActionResult GetPagerResult(ClientResult result, int pno, int pagesize,string modelName)
        {
            if (result.code == 1)
            {
                var list = ApiHelper.Response(result);
                //自动生成model文件，默认/Model/auto文件夹
                if (!string.IsNullOrEmpty(modelName) && list != null && list.Count>0)
                {
                    string path = (AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\') + @"\").ToLower().Replace(@"\bin\debug/",@"\").Replace(@"\bin\",@"\") + @"Models\auto\";
                    new Assemblys.AutoModelList().AutoCode(path + modelName + ".cs", list[0], this.GetType().Namespace.Replace(".Controllers", ".Model"), modelName);
                }
                PagedList<dynamic> pagelist = new Webdiyer.WebControls.Mvc.PagedList<dynamic>(list, pno, pagesize, (int)result.total);
                if (Request.IsAjaxRequest())
                    return PartialView("List", pagelist);
                return View(pagelist);
            }
            return View();

        }


    }
}
