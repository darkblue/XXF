﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using XXF.BasicService.ConfigCenter;

namespace XXF.BasicService.ConfigCenter
{
    public class CCHelper
    {
        public static T ConfigHelper<T>(string xmdh, string csmc)
        {
            try
            {
                ConfigCenterProvider config = new ConfigCenterProvider();
                object obj = config.CallApiGetC(xmdh, csmc);
                if (obj == "")
                {
                    return default(T);
                }
                T ou = (T)obj;
                return ou;
            }
            catch
            {
                return default(T);
            }
        }
    }
}
