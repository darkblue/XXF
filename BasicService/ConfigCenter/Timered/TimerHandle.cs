﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Newtonsoft.Json.Linq;
using XXF.Api;
using System.Configuration;

namespace XXF.BasicService.ConfigCenter.Timered
{
    public class TimerHandle
    {
        Timer time;
        public string updatetime = "";

        public void CreateTime(double In)
        {
            time = new Timer();
            time.Interval = In;
            time.AutoReset = true;
            time.Elapsed += new System.Timers.ElapsedEventHandler(TimeToChange);
            time.Start();
        }

        /// <summary>初始化</summary>
        public void init()
        {
            if (time == null)
            {
                List<Api.ParmField> Par = new List<ParmField>();
                Api.ClientResult result = HttpServer.InvokeApi("/ConfigApi/GetAll", Par);
                if (result.code > 0)
                {
                    double In = 3000;
                    if (result.repObject["response"].ToString() != "")
                    {
                        ConfigCenter.Cached.ConfigsModel configs = GetConfigModel((JObject)result.repObject["response"]);
                        Cached.CachedHandle.UpOrAddCache(configs);
                        ConfigCenter.Xml.XmlHandle.Instance.CreateXml(Cached.CachedHandle.mother.Clone());
                        updatetime = result.repObject["response"]["UpTime"].ToString();
                        CreateTime(In);
                    }
                }
                else
                {
                    throw new Exception("初始化失败！");
                }
            }
        }
        /// <summary>得到ConfigModel</summary>
        /// <param name="jo"></param>
        /// <returns></returns>
        private ConfigCenter.Cached.ConfigsModel GetConfigModel(JObject jo)
        {
            ConfigCenter.Cached.ConfigsModel Configs = new Cached.ConfigsModel();
            Configs.Configs = new List<Cached.Config>();
            Configs.Update = jo["UpTime"].ToString();
            JArray ja=JArray.Parse(jo["data"].ToString());
            foreach (JToken jt in ja)
            {
                string csz = ConfigurationManager.AppSettings[jt["csmc"].ToString()];
                if (csz != null)
                {
                    Configs.Configs.Add(new Cached.Config()
                    {
                        csmc = jt["csmc"].ToString(),
                        csz = csz,
                        xmdh = jt["xmdh"].ToString()
                    });
                }
                else
                {
                    Configs.Configs.Add(new Cached.Config()
                    {
                        csmc = jt["csmc"].ToString(),
                        csz = jt["csz"].ToString(),
                        xmdh = jt["xmdh"].ToString()
                    });
                }
            }
            return Configs;
        }

        /// <summary>自动更新</summary>
        /// <param name="source"></param>
        /// <param name="e"></param>
        public void TimeToChange(object source, ElapsedEventArgs e)
        {
            List<Api.ParmField> Par = new List<Api.ParmField>();
            Par.Add(new Api.StringField("updatetime", updatetime));
            Api.ClientResult result = HttpServer.InvokeApi("/ConfigApi/GetAll", Par);
            if (result.repObject["response"].ToString() != "")
            {
                updatetime = result.repObject["response"]["UpTime"].ToString();
                ConfigCenter.Cached.ConfigsModel configs = GetConfigModel((JObject)result.repObject["response"]);
                Cached.CachedHandle.UpOrAddCache(configs);
                ConfigCenter.Xml.XmlHandle.Instance.CreateXml(Cached.CachedHandle.mother.Clone());

                //如果调度时间改变，更新
                ConfigCenter.Cached.Config config = configs.Configs.FirstOrDefault(o => o.xmdh == "Config" && o.csmc == "BackTime");
                if (config != null)
                {
                    time.Stop();
                    time.Interval = Convert.ToDouble(config.csz);
                    time.Start();
                }
            }
        }
    }
}
