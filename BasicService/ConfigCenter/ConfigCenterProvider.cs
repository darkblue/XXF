﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XXF.Api;
using Newtonsoft.Json.Linq;
using System.Configuration;
using XXF.BasicService.ConfigCenter.Cached;

namespace XXF.BasicService.ConfigCenter
{
    public class ConfigCenterProvider
    {
        private string url = "http://localhost:8060";

        static ConfigCenterProvider()
        {
            try
            {
                Timered.TimerHandle init = new Timered.TimerHandle();
                init.init();
            }
            catch { }
        }

        /// <summary>获取一个配置参数</summary>
        /// <param name="xmdh"></param>
        /// <param name="csmc"></param>
        /// <param name="cgapp">是否修改AppSetting</param>
        /// <returns></returns>
        public string CallApiGetC(string xmdh, string csmc)
        {
            string result = GetOneForConfig(xmdh, csmc);
            return result;
        }

        /// <summary>得到一个项目的参数</summary>
        /// <param name="xmdh"></param>
        /// <returns></returns>
        public List<Config> CallApiGetC(string xmdh)
        {
            List<Config> result = GetAllForConfig(xmdh);
            return result;
        }

        /// <summary>获取单个项目所有信息</summary>
        /// <param name="xmdh"></param>
        /// <returns></returns>
        private List<Config> GetAllForConfig(string xmdh)
        {
            List<Config> config = new List<Config>();
            config = Cached.CachedHandle.GetConfigCacheList(xmdh);
            if (config == null)
                config = Xml.XmlHandle.Instance.GetConfigXmlList(xmdh);
            if (config == null)
            {
                List<Api.ParmField> Par = new List<Api.ParmField>();
                Par.Add(new Api.StringField("xmdh", xmdh));
                Api.ClientResult result = HttpServer.InvokeApi(url + "/ConfigApi/GetAllForPro", Par);
                if (result.code > 0 && result.repObject["response"].ToString() != "")
                {
                    JArray ja = JArray.Parse(result.repObject["response"].ToString());
                    foreach (JToken jt in ja)
                    {
                        config.Add(new Config()
                        {
                            csmc = jt["csmc"].ToString(),
                            csz = jt["csz"].ToString(),
                            xmdh = jt["xmdh"].ToString()
                        });
                    }
                }
                else
                {
                    config = null;
                }
            }
            return config;
        }

        /// <summary>获取指定配置信息</summary>
        /// <param name="xmdh"></param>
        /// <param name="csmc"></param>
        /// <returns></returns>
        private string GetOneForConfig(string xmdh, string csmc)
        {
            string config = null;
            config = Cached.CachedHandle.GetConfigCache(csmc, xmdh);
            if (config == null)
                config = Xml.XmlHandle.Instance.GetConfigXml(csmc, xmdh);
            if (config == null)
            {
                List<Api.ParmField> Par = new List<Api.ParmField>();
                Par.Add(new Api.StringField("xmdh", xmdh));
                Par.Add(new Api.StringField("csmc", csmc));
                Api.ClientResult result = HttpServer.InvokeApi(url + "/ConfigApi/GetOneForPro", Par);
                if (result.code < 0 && result.repObject["response"].ToString() != "")
                {
                    config = result.repObject["response"].ToString();
                }
                else
                {
                    config = null;
                }
            }
            return config;
        }
    }
}
