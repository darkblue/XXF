﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BasicService.MonitorPlatform
{
    /// <summary>
    /// 平台监控帮助类
    /// 后续需要加入缓冲层,进行批量写，减少数据层压力
    /// </summary>
    public class MonitorPlatformHelper
    {
        /// <summary>
        /// 添加错误日志
        /// </summary>
        /// <param name="errorinfo"></param>
        public static void AddErrorLog(ErrorInfo errorinfo)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(XXF.Common.XXFConfig.MonitorPlatformConnectionString))
                {
                    using (var c = Db.DbConfig.CreateConn(Db.DbType.SQLSERVER, XXF.Common.XXFConfig.MonitorPlatformConnectionString))
                    {
                        c.IsWatchTime = false;
                        c.Open();
                        new Dal.tb_ErrorLog_dal().Add(c, errorinfo);
                    }
                }
                //MonitorPlatform.Dal.QueueDal.AdderrorInfoQueue(errorinfo);
            }
            catch (Exception exp) { System.Diagnostics.Debug.WriteLine("监控平台打印出错:" + exp.Message); }
        }
        /// <summary>
        /// 添加耗时日志
        /// </summary>
        /// <param name="timewatchinfo"></param>
        public static void AddTimeWatchLog(TimeWatchInfo timewatchinfo)
        {
            try
            {
                MonitorPlatform.Dal.QueueDal.AddtimeWatchInfoQueue(timewatchinfo);
            }
            catch (Exception exp) { System.Diagnostics.Debug.WriteLine("监控平台打印出错:" + exp.Message); }
        }
    }

    /// <summary>
    /// 错误信息
    /// </summary>
    public class ErrorInfo : Model.tb_ErrorLog_model
    {

    }
    /// <summary>
    /// 耗时信息
    /// </summary>
    public class TimeWatchInfo : Model.tb_TimeWatchLog_model
    {

    }
}
