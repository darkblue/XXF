﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace XXF.BasicService.NotifyCenter
{
    /// <summary>
    /// 消息中心 消息发送目标信息
    /// </summary>
    public class NCCustomerInfo
    {

        public NCCustomerInfo()
        {
        }
        /// <summary>
        /// 用户信息
        /// </summary>
        /// <param name="mobileLink">手机号</param>
        /// <param name="emailLink">邮件地址</param>
        /// <param name="phonePushLink">推送唯一设备号</param>
        /// <param name="QQLink">qq号码</param>
        /// <param name="yhzh">用户账号</param>
        public NCCustomerInfo(string mobileLink, string emailLink, PhonePushLinkModel phonePushLink, string QQLink)
        {
            this.MobileLink = mobileLink;
            this.EmailLink = emailLink;
            this.PhonePushLink = phonePushLink;
            this.QQLink = QQLink;
        }

        /// <summary>
        /// 手机联系方式
        /// </summary>
        public string MobileLink { get; set; }

        /// <summary>
        /// 邮件地址
        /// </summary>
        public string EmailLink { get; set; }
        
        /// <summary>
        /// 手机推送联系 int手机类型 string 手机appid
        /// </summary>
        public PhonePushLinkModel PhonePushLink { get; set; }

        /// <summary>
        /// QQ号
        /// </summary>
        public string QQLink { get; set; }

    }

    public class PhonePushLinkModel
    {
        /// <summary>
        /// 设备类型 ios=1 andserial=2
        /// </summary>
        public int DeviceType { get; set; }

        /// <summary>
        /// 设备唯一标识
        /// </summary>
        public string AppInfo { get; set; }
      
        /// <summary>
        /// 用户类型 0用户 1商户 2用户网上下载
        /// </summary>
        public int CutomerType { get; set; }
    }
}
